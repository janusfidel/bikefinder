//
//  LocalAuthManager.swift
//  POCTouchID
//
//  Created by Janus fidel Balatbat on 5/27/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit
import LocalAuthentication
import Security

// Identifiers
var serviceID = "ph.com.bikefinder"
let account = "userAuthentication"

let kSecClassV = NSString(format: kSecClass)
let kSecAttrAccountV = NSString(format: kSecAttrAccount)
let kSecValueDataV = NSString(format: kSecValueData)
let kSecClassGenericPasswordV = NSString(format: kSecClassGenericPassword)
let kSecAttrServiceV = NSString(format: kSecAttrService)
let kSecMatchLimitV = NSString(format: kSecMatchLimit)
let kSecReturnDataV = NSString(format: kSecReturnData)
let kSecMatchLimitOneV = NSString(format: kSecMatchLimitOne)

class LocalAuthManager: NSObject {
    
    /*
     Functions and logic for dealing with TouchID
     */
    class func canAuthenticateUserWithTouchID()->(canAuthenticate: Bool, reasonForFailure:NSError?){
        var error: NSError?
        let context = LAContext()
        if context.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, error: &error) {
            return (true, nil)
        }else{
            return (false, error)
        }
    }
    
    class func authenticateUser(reason:String,completion:(success: Bool,error:NSError?)-> Void){
        let context = LAContext()
        [context .evaluatePolicy(LAPolicy.DeviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply: { (success: Bool, evalPolicyError: NSError?) -> Void in
            if success {
                completion(success: true,error: nil)
            }
            else{
                completion(success: false,error: evalPolicyError)
            }
            
        })]
        
        
    }
    /*
     Functions and logic for saving and retrieving password.
     */
    
    //exposed methods
    internal class func saveToken(token: NSString) {
        self.save(serviceID, data: token)
    }
    
    internal class func loadToken() -> NSString? {
        let token = self.load(serviceID)
        return token
    }
    
    private class func save(service: NSString, data: NSString) {
        let dataFromString: NSData = data.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
        
        // Instantiate a new default keychain query
        let query: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordV, service, account, dataFromString], forKeys: [kSecClassV, kSecAttrServiceV, kSecAttrAccountV, kSecValueDataV])

        SecItemDelete(query as CFDictionaryRef)
        
        let status: OSStatus = SecItemAdd(query as CFDictionaryRef, nil)
        print("save token status : \(status)")
    }
    
    
    private class func load(service: NSString) -> NSString? {
        let query: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPassword, service, account, kCFBooleanTrue, kSecMatchLimitOneV], forKeys: [kSecClassV, kSecAttrServiceV, kSecAttrAccountV, kSecReturnDataV, kSecMatchLimitV])
        
        var dataTypeRef :AnyObject?
        // Search for the keychain items
        let status: OSStatus = withUnsafeMutablePointer(&dataTypeRef) { SecItemCopyMatching(query as CFDictionaryRef, UnsafeMutablePointer($0)) }
        if status == noErr {
            print("NO ERR")
            let contentsOfKeychain = NSString(data: dataTypeRef as! NSData, encoding: NSUTF8StringEncoding)!
            print("contentsOfKeychain  =\(contentsOfKeychain)")
            return contentsOfKeychain
        }
        else {
            return nil
        }
        
        
        
    }

    
    
    
    
}
