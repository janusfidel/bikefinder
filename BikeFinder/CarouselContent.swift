//
//  CarouselModel.swift
//  mySmart
//
//  Created by Janus fidel Balatbat on 3/9/16.
//  Copyright © 2016 Smart Communications, Inc. All rights reserved.
//

import UIKit

class CarouselContent: UIView {
    var view: UIView!
    var model = CarouselModel(){
        didSet{
            self.config()
        }
    }
    @IBOutlet weak var noteLbl: UILabel!
    
    func config(){
        self.noteLbl.text = model.notes
    }
    
    //MARK: Initializers
    override init(frame: CGRect) {
        super.init(frame: frame)
        nib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nib()
    }
    
    func nib() {
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "CarouselContent", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
    
    
    
    
    
    

}
