//
//  ViewController.swift
//  BikeFinder
//
//  Created by Janus fidel Balatbat on 6/2/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit
    private struct Sources {
        static let kWelcomeDataPlistSource = "welcome_ref"
    }
    private struct DataKeys {
        static let kBg = "bg"
        static let kNote = "text"
    }
class ViewController: UIViewController ,CarouselViewDelegate{
    @IBOutlet weak var carouselView : CarouselView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*setting the Welcome presentation*/
        let pathResource : String = NSBundle.mainBundle().pathForResource(Sources.kWelcomeDataPlistSource, ofType: "plist")!
        let itemRef : NSArray = NSArray(contentsOfFile: pathResource)! as! [[String: String]]
        var pages = [CarouselModel]()
        for item in itemRef {
            let model = CarouselModel(bgImage: UIImage(named: item[DataKeys.kBg] as! String), note: item[DataKeys.kNote] as? String)
            pages.append(model)
        }
        self.carouselView.pagesArray = pages
        self.carouselView.delegate = self
        
    }
    func onButtonPressed(button: UIButton) {
        //get started
        let loginController = LoginViewController(nibName: "LoginViewController", bundle: nil)
        let loginNavCon = UINavigationController(rootViewController: loginController)
        loginNavCon.navigationBar.hidden = true;
        self.presentViewController(loginNavCon, animated: true) { 
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

