//
//  BikePlace.swift
//  BikeFinder
//
//  Created by Janus fidel Balatbat on 6/3/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit

class BikePlace: NSObject {
    var identifier : String?
    var name : String?
    var latitude : Double?
    var longitude : Double?
    override init() {
        super.init()
    }
    init(data: [String : AnyObject]) {
        super.init()
        if let o = data["id"] as? String{
            self.identifier = o
        }
        if let o = data["name"] as? String{
            self.name = o
        }
        if let o = data["location"] as? [String : AnyObject]{
            self.latitude = o["lat"] as? Double
            self.longitude = o["lng"] as? Double
        }
        
    }
}
