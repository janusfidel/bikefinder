//
//  CarouselModel.swift
//  mySmart
//
//  Created by Janus fidel Balatbat on 3/9/16.
//  Copyright © 2016 Smart Communications, Inc. All rights reserved.
//

import UIKit

class CarouselModel: NSObject {
    var backgroundImage: UIImage?
    var centerImage : UIImage?
    var notes: String?
    
    override init() {
        super.init()
    }
    init(bgImage: UIImage?,note: String?){
        super.init()
        if let bg = bgImage{
            self.backgroundImage = bg
        }
        if let text = note{
            self.notes = text
        }
        
    }
    
    /*un-used,  no need for center image in UI*/
    init(centerImage: UIImage?, bgImage: UIImage?,note: String?){
        super.init()
        if let bg = bgImage{
            self.backgroundImage = bg
        }
        if let center = centerImage{
            self.centerImage = center
        }
        if let text = note{
            self.notes = text
        }
        
    }
}
