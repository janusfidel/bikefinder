//
//  Constants.swift
//  BikeFinder
//
//  Created by Janus fidel Balatbat on 6/20/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

//HTTP
import Foundation

let BASE_URL = "http://localhost:8080/api/v1/"
let HTTP_DEFAULT_TIMEOUT : NSTimeInterval = 15

//GOOGLEMAPS
let GMAP_API_KEYS = "AIzaSyDRS9NDVMMvxSiexBU40wEQS7Xbj1nPt7U"



struct ErrorMessages {
    static let kErrorPromptTitle = "Error"
    struct TextInputs {
        static let kInvalidEmail = "INVALID_EMAIL"
        static let kAllFieldsRequired = "ALL_FIELDS_REQUIRED"
        static let kInvalidCCNumber = "INVALID_CARD_NUMBER"
        
    }
}
struct NavigationItemsTitle {
    static let AppHome = "Bicycles"
}