//
//  CarouselView.swift
//  mySmart
//
//  Created by Janus fidel Balatbat on 3/9/16.
//  Copyright © 2016 Smart Communications, Inc. All rights reserved.
//

import UIKit
protocol CarouselViewDelegate{
 func onButtonPressed(button: UIButton)
}
class CarouselView: UIView ,UIScrollViewDelegate{
    var delegate : CarouselViewDelegate?
    var currentPhotoIndex = -1;
    @IBOutlet weak var pageControl : UIPageControl!
    @IBOutlet weak var bgImage1 : UIImageView!
    @IBOutlet weak var bgImage2 : UIImageView!
    @IBOutlet weak var scrollView : UIScrollView!
    var pagesArray = [CarouselModel](){
        didSet{
            self.configContents()
        }
        
    }
    
    var view: UIView!
    //MARK: Actions
    @IBAction func onStart(btn: UIButton){
        if let del = self.delegate{
            del.onButtonPressed(btn)
        }
    }
    //MARK: Config
    func configContents(){
        self.setNeedsLayout()
        self.layoutIfNeeded()
        var scrollViewCSwidth : CGFloat = 0.0;
        for model in self.pagesArray{
            let content = CarouselContent(frame: CGRectMake(scrollViewCSwidth, 0, self.scrollView.frame.size.width, self.scrollView.frame.size.height))
            scrollViewCSwidth += self.scrollView.frame.size.width
            content.model = model
            self.scrollView.addSubview(content)
        }
        self.scrollView.contentSize = CGSizeMake(scrollViewCSwidth, 0)
        self.updateView()
        self.pageControl.numberOfPages = self.pagesArray.count
    }
    func updateView(){
        let scrollPhotoIndx = max(0, min(pagesArray.count-1, Int((scrollView.contentOffset.x / self.frame.size.width))))
        if(scrollPhotoIndx != currentPhotoIndex) {
            currentPhotoIndex = scrollPhotoIndx;
            print(pagesArray[currentPhotoIndex].backgroundImage)
            bgImage1.image = pagesArray[currentPhotoIndex].backgroundImage
            bgImage2.image = currentPhotoIndex + 1 != pagesArray.count ? pagesArray[currentPhotoIndex+1].backgroundImage : nil
        }
            var offSet = scrollView.contentOffset.x - (CGFloat(currentPhotoIndex) * self.frame.size.width)

            if offSet < 0{
                pageControl.currentPage = 0;
                offSet = self.frame.size.width - min(-offSet, self.frame.size.width)
                bgImage2.alpha = 0;
                bgImage1.alpha = (offSet/self.frame.size.width)

            }else if (offSet != 0){
                if (scrollPhotoIndx == pagesArray.count - 1){
                    pageControl.currentPage = pagesArray.count-1
                    bgImage1.alpha = 1.0 - (offSet / self.frame.size.width)
                }else{
                    pageControl.currentPage = (offSet > self.frame.size.width/2) ? currentPhotoIndex + 1 : currentPhotoIndex
                    bgImage2.alpha = offSet / self.frame.size.width
                    bgImage1.alpha = 1.0 - bgImage2.alpha
                }
            }else{
                pageControl.currentPage = currentPhotoIndex;
                bgImage1.alpha = 1;
                bgImage2.alpha = 0;
            }

    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
            self.updateView()
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
            self.updateView()
    }
    //MARK: Layout
     override func layoutSubviews() {
            super.layoutSubviews()
    }
    
    //MARK: Initializers
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        setUp()
    }
    func setUp() {
        view = loadNib()
        view.frame = bounds
        view.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        self.bgImage2.contentMode = UIViewContentMode.ScaleAspectFill
        self.bgImage2.autoresizingMask = [UIViewAutoresizing.FlexibleHeight , UIViewAutoresizing.FlexibleWidth]
        self.bgImage1.contentMode = UIViewContentMode.ScaleAspectFill
        self.bgImage1.autoresizingMask = [UIViewAutoresizing.FlexibleHeight , UIViewAutoresizing.FlexibleWidth]

        
        addSubview(view)
    }
    func loadNib() -> UIView {
        let bundle = NSBundle(forClass: self.dynamicType)
        let nib = UINib(nibName: "CarouselView", bundle: bundle)
        let view = nib.instantiateWithOwner(self, options: nil)[0] as! UIView
        return view
    }
  


}
