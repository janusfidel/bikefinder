//
//  LoginViewController.swift
//  BikeFinder
//
//  Created by Janus fidel Balatbat on 6/2/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit

class LoginViewController: UserEntryViewController,UITextFieldDelegate {
    @IBOutlet weak var emailFld: UITextField!
    @IBOutlet weak var passwordTxtFld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addAllTextFieldsAsObserver()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions
    @IBAction func onLogin(){
        //validate inputs
        if self.emailFld.text?.isEmpty == true || self.passwordTxtFld.text?.isEmpty == true{
            Globals.showMessageDialog(ErrorMessages.kErrorPromptTitle, message: NSLocalizedString(ErrorMessages.TextInputs.kAllFieldsRequired, comment: ""), completion: { (done) in
                
            })
            return;
        }
        //start api call
        APICalls.loginEmail(self.emailFld.text!, password: self.passwordTxtFld.text!, completion: { (token, error) in
            if token != nil{
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                    //save token to keychain
                    LocalAuthManager.saveToken(token!)
                    dispatch_async(dispatch_get_main_queue(), {
                        //show application main content
                        self.loginUser()
                    })
                })
              
            }else{
                Globals.showMessageDialog(ErrorMessages.kErrorPromptTitle, message: error.message!, completion: { (done) in
                })
                
            }
        })
    }
    
    @IBAction func onCreateAccount(){
        let registerController = RegisterViewController(nibName: "RegisterViewController", bundle: nil)
        self.navigationController?.pushViewController(registerController, animated: true)
    }
    
    @IBAction func onForgotPassword(){
    
    }
    //MARK: TextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(textField: UITextField) {
        textField.resignFirstResponder()
    }
    

    
}
