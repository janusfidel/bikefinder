//
//  APICalls.swift
//  BikeFinder
//
//  Created by Janus fidel Balatbat on 6/2/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit


class APICalls: NSObject {
    //MARK: USER LOGIN
    class func loginEmail(email:String, password:String,completion:(token: String?,error:(code:Int?, message:String?))->Void)
    {
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let paramString = "email=\(email)&password=\(password)"
        let url:NSURL = NSURL(string: "\(BASE_URL)auth")!
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
        
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            if error != nil{
                completion(token:nil, error: (code:error?.code,message:error?.localizedDescription))
            }else{
                if let res = response as? NSHTTPURLResponse{
                    let jsonRes = JSON(data: data!)
                    if res.statusCode >= 200 &&  res.statusCode <= 300{
                        let token = jsonRes["accessToken"].stringValue
                        completion(token:token, error: (code:0,message:nil))
                    }else{
                        let errorMessage = jsonRes["message"].stringValue
                        completion(token:nil, error: (code:res.statusCode,message:errorMessage))
                    }
                }else{
                    print("no response")
                }
            }
        });
        task.resume()
        
    }
    //MARK: USER REGISTRATION
    class func registerUserWithEmail(email:String, password:String,completion:(token: String?,error:(code:Int?, message:String?))->Void)
    {
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let paramString = "email=\(email)&password=\(password)"
        let url:NSURL = NSURL(string: "\(BASE_URL)register")!
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-type")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        request.HTTPBody = paramString.dataUsingEncoding(NSUTF8StringEncoding)
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            if error != nil{
                completion(token:nil, error: (code:error?.code,message:error?.localizedDescription))
            }else{
                if let res = response as? NSHTTPURLResponse{
                    let jsonRes = JSON(data: data!)
                    if res.statusCode >= 200 &&  res.statusCode <= 300{
                        let token = jsonRes["accessToken"].stringValue
                        completion(token:token, error: (code:0,message:nil))
                    }else{
                        let errorMessage = jsonRes["message"].stringValue
                        completion(token:nil, error: (code:res.statusCode,message:errorMessage))
                    }
                }else{
                    print("no response")
                }
            }
        });
        task.resume()
        
    }
    
    //MARK: GET LIST OF BICYCLES
    class func getBicycles(completion:(places: [BikePlace]?,error:(code:Int?, message:String?))->Void)
    {
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let url:NSURL = NSURL(string: "\(BASE_URL)places")!
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        request.addValue(LocalAuthManager.loadToken() as! String, forHTTPHeaderField: "Authorization")
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            if error != nil{
                completion(places:nil, error: (code:error?.code,message:error?.localizedDescription))
            }else{
                if let res = response as? NSHTTPURLResponse{
                    let jsonRes = JSON(data: data!)
                    if res.statusCode >= 200 &&  res.statusCode <= 300{
                        var placesArray = [BikePlace]()
                        let placesRes = jsonRes["results"].object as? [[String : AnyObject]]
                        if let places = placesRes{
                            for item in places{
                                let bikePlace = BikePlace(data: item)
                                placesArray.append(bikePlace)
                            }
                        }
                        completion(places: placesArray, error: (nil,nil))
                    }else{
                        let errorMessage = jsonRes["message"].stringValue
                        completion(places:nil, error: (code:res.statusCode,message:errorMessage))
                    }
                }else{
                    print("no response")
                }
            }
        });
        task.resume()

    }
    class func submitPaymentInfoWithDetails(details:[String : String],completion:(message: String?,error:(code:Int?, message:String?))->Void){
        let params = details.queryStringWithEncoding()
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: config)
        let url:NSURL = NSURL(string: "\(BASE_URL)rent")!
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.HTTPBody = params.dataUsingEncoding(NSUTF8StringEncoding)
        request.addValue(LocalAuthManager.loadToken() as! String, forHTTPHeaderField: "Authorization")
        let task = session.dataTaskWithRequest(request, completionHandler: {(data, response, error) in
            if error != nil{
                completion(message: nil, error: (code: error?.code, message: error?.localizedDescription))
            }else{
                if let res = response as? NSHTTPURLResponse{
                    let jsonRes = JSON(data: data!)
                    if res.statusCode >= 200 &&  res.statusCode <= 300{
                        let message = jsonRes["message"].stringValue
                        completion(message: message, error: (nil,nil))
                    
                    }else{
                        let errorMessage = jsonRes["message"].stringValue
                        completion(message:nil, error: (code:res.statusCode,message:errorMessage))
                    }
                }else{
                    print("no response")
                }
            }
        });
        task.resume()
        

    }
 
}
