//
//  UserEntryViewController.swift
//  BikeFinder
//
//  Created by Janus fidel Balatbat on 6/2/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit

class UserEntryViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    internal func loginUser(){
        let bikeBrowserController = BrowseBicyclesViewController(nibName: "BrowseBicyclesViewController", bundle: nil)
        let baseView = UINavigationController(rootViewController: bikeBrowserController)
        UIView.transitionWithView(self.view, duration: 1.0, options: UIViewAnimationOptions.TransitionCrossDissolve, animations: {
            self.view.alpha = 0.0; },
                                  completion: {complete in
                                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate;
                                    appDelegate.window?.rootViewController = nil
                                    appDelegate.window?.rootViewController = baseView;
                                    
        })
        
    }

}
