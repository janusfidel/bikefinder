//
//  Extensions.swift
//  BikeFinder
//
//  Created by Janus fidel Balatbat on 6/2/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit
//MARK:DICTIONARY
public extension Dictionary{
    func queryStringWithEncoding() -> String {
        var parts = [String]()
        
        for (key, value) in self {
            let keyString: String = "\(key)"
            let valueString: String = "\(value)"
            let query: String = "\(keyString)=\(valueString)"
            parts.append(query)
        }
        
        return parts.joinWithSeparator("&")
    }
}




//MARK: UIApplication
public extension UIApplication {
    class func topViewController(base: UIViewController? = UIApplication.sharedApplication().keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        
        if let tab = base as? UITabBarController {
            let moreNavigationController = tab.moreNavigationController
            
            if let top = moreNavigationController.topViewController where top.view.window != nil {
                return topViewController(top)
            } else if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        
        if let presented = base?.presentedViewController {
            return topViewController(presented)
        }
        
        return base
    }
}

//MARK: UICOLOR
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(netHex:Int) {
        self.init(red:(netHex >> 16) & 0xff, green:(netHex >> 8) & 0xff, blue:netHex & 0xff)
}
}
//MARK: UIBUTTON
public extension UIButton{
    public func startBtnLoading(){
        self.enabled = false
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        let halfButtonHeight = self.bounds.size.height / 2
        let buttonWidth = self.bounds.size.width
        activityIndicator.center = CGPointMake(buttonWidth - halfButtonHeight, halfButtonHeight)
        self.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
    }
    
    public func stopBtnLoading(){
        self.enabled = true
        for view in self.subviews{
            if view.isKindOfClass(UIActivityIndicatorView){
                let activityVw = view as! UIActivityIndicatorView
                activityVw.stopAnimating()
                view.removeFromSuperview()
            }
        }
        
    }
}
//MARK: UIVIEW
public extension UIView {
    //can be used to add all textfields in view for repositioning when keyboard is shown
    public func addAllTextFieldsAsObserver() {
        for textField in self.getAllTextFieldsInView(self){
            let nsnotif = NSNotificationCenter.defaultCenter()
            nsnotif.removeObserver(textField)
            nsnotif.addObserver(textField, selector: #selector(textField.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
            nsnotif.addObserver(textField, selector: #selector(textField.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
            
        }
    }

    public func removeAllTextFieldsAsObserver() {
        for textField in self.getAllTextFieldsInView(self) {
            let nsnotif = NSNotificationCenter.defaultCenter()
            nsnotif.removeObserver(textField)
        }
    }
    
    public func getAllTextFieldsInView(view: UIView) -> [UITextField] {
        var results = [UITextField]()
        for subview in view.subviews as [UIView] {
            if let tfView = subview as? UITextField {
                results += [tfView]
            } else {
                results += getAllTextFieldsInView(subview)
            }
        }
        return results
        
    }
    
  }
//MARK: UITextFields
public extension UITextField {
    func isInputValidEmail()->Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        let emailTest  = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(self.text)
    }
    
    func keyboardWillShow(notif: NSNotification) {
        if let userInfo = notif.userInfo {
            let keyboardFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as! NSValue).CGRectValue()
            if self.isFirstResponder() == false{
                return
            }
            self.keyboardWillHide(notif)

            let sView = self.superview
            let rootView = UIApplication.topViewController()?.view
            
            let txtFieldBottomPoint: CGPoint = CGPointMake(self.frame.origin.x, self.frame.origin.y + self.frame.size.height)
            let newTxtFieldBottomPoint: CGPoint = sView!.convertPoint(txtFieldBottomPoint, toView: rootView)
            
            if newTxtFieldBottomPoint.y < keyboardFrame.origin.y {
                return;
            }
            let topPadding : CGFloat = 10
            
            let newTargetPoint: CGPoint = CGPointMake(self.frame.origin.x, keyboardFrame.origin.y - topPadding)
            let targetPointOffset: CGFloat = newTargetPoint.y - newTxtFieldBottomPoint.y
            
            let adjustedViewFrameCenter: CGPoint = CGPointMake(rootView!.center.x, rootView!.center.y + targetPointOffset)
            
            UIView.animateWithDuration(0.2, animations:  {
                rootView?.center = adjustedViewFrameCenter
            })
        }
        
    }
    
    func keyboardWillHide(notif: NSNotification) {
        let rootView = UIApplication.topViewController()?.view
        let originalVwRect: CGRect = CGRectMake(0.0, 0.0, rootView!.frame.size.width, rootView!.frame.size.height)
        UIView.animateWithDuration(0.2, animations: {
            rootView!.frame = originalVwRect
        });
    }
    

}
