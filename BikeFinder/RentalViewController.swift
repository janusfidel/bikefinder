//
//  RentalViewController.swift
//  BikeFinder
//
//  Created by Janus fidel Balatbat on 6/3/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit


private struct DataKeys {
        static let kCCNumber = "number"
        static let kCCName = "name"
        static let kCCExpiration = "expiration"
        static let kCCCode = "code"
}


class RentalViewController: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var ccName: UITextField!
    @IBOutlet weak var ccCode: UITextField!
    @IBOutlet weak var ccExpiry: UITextField!
    @IBOutlet weak var ccNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addAllTextFieldsAsObserver()
        Globals.addDonebuttonToTextField(self.ccCode, selector: #selector(RentalViewController.dismissKeyboard), viewCon: self)
        Globals.addDonebuttonToTextField(self.ccNumber, selector: #selector(RentalViewController.dismissKeyboard), viewCon: self)
        
    }
    //MARK: Actions
    @IBAction func onSubmitPayment(sender:UIButton){
        sender.startBtnLoading()
        for tf in self.view.getAllTextFieldsInView(view){
            if tf.text?.isEmpty == true{
                Globals.showMessageDialog(ErrorMessages.kErrorPromptTitle, message: NSLocalizedString(ErrorMessages.TextInputs.kAllFieldsRequired, comment: ""), completion: { (done) in
                })
                return
            }
        }
        if self.ccNumber.text?.characters.count != 16{
            Globals.showMessageDialog(ErrorMessages.kErrorPromptTitle, message: NSLocalizedString(ErrorMessages.TextInputs.kInvalidCCNumber, comment: ""), completion: { (done) in
                
            })
            return
        }
        
        APICalls.submitPaymentInfoWithDetails([DataKeys.kCCNumber: self.ccNumber.text! ,DataKeys.kCCName: self.ccName.text! ,DataKeys.kCCExpiration: self.ccExpiry.text! ,DataKeys.kCCCode: self.ccCode.text! ]) {
            (message, error) in
            sender.stopBtnLoading()
            if let msg = message{
                Globals.showMessageDialog("", message: msg, completion: { (done) in
                    self.navigationController?.popViewControllerAnimated(true)
                })
            }else{
                Globals.showMessageDialog(ErrorMessages.kErrorPromptTitle, message: error.message!, completion: { (done) in
                })
                
            }
            
        }
    }
    
    func  dismissKeyboard() -> Void {
        ccCode.resignFirstResponder()
        ccNumber.resignFirstResponder()
    }
    //MARK:Memory
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK:TextFieldDelegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
