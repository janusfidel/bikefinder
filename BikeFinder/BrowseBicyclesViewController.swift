//
//  BrowseBicyclesViewController.swift
//  BikeFinder
//
//  Created by Janus fidel Balatbat on 6/2/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit
import GoogleMaps

class BrowseBicyclesViewController: UIViewController,GMSMapViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NavigationItemsTitle.AppHome
        self.getBikes()
        
        //set up GoogleMaps
           }

    func  getBikes() -> Void {
        APICalls.getBicycles { (places, error) in
            if let bikes = places{
                dispatch_async(dispatch_get_main_queue(), {
                    self.dropMapsForBikes(bikes)
                })
                
            }
            
        }
    }
    func  dropMapsForBikes(bikes: [BikePlace]) -> Void {
        GMSServices.provideAPIKey(GMAP_API_KEYS)
        let camera = GMSCameraPosition.cameraWithLatitude(bikes[0].latitude!,
                                                          longitude: bikes[0].longitude!, zoom: 10)
        let mapView = GMSMapView.mapWithFrame(CGRectZero, camera: camera)
        mapView.myLocationEnabled = true
        self.view = mapView
        mapView.delegate = self
        for bike in bikes{
            let marker = GMSMarker()
            marker.icon = UIImage(named: "ic-bike-marker")
            marker.position = CLLocationCoordinate2DMake(bike.latitude!, bike.longitude!)
            marker.title = bike.name
            marker.map = mapView
        }
    }
    
    func mapView(mapView: GMSMapView, didTapInfoWindowOfMarker marker: GMSMarker){
        //InfoWindow was tapped. 
        //show rental page
        let rentalController = RentalViewController(nibName: "RentalViewController", bundle: nil)
        self.navigationController?.pushViewController(rentalController, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
