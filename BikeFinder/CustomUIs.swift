//
//  CustomUIs.swift
//  BikeFinder
//
//  Created by Janus fidel Balatbat on 6/2/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit


public class UIButtonCurveBorder: UIButton {
    override public func setNeedsDisplay() {
        super.setNeedsDisplay()
        self.tintColor = UIColor.whiteColor()
        self.backgroundColor = UIColor.clearColor()
        self.layer.cornerRadius = 5.0
        self.layer.borderColor = UIColor.whiteColor().CGColor
        self.layer.borderWidth = 0.5
    }
    
}
public class UIButtonUnderline : UIButton {
    override public func awakeFromNib() {
        super.awakeFromNib()
        let attributes : [String : AnyObject] = [NSUnderlineStyleAttributeName : 1]
        let attributedString : NSAttributedString = NSAttributedString(string: self.titleLabel!.text!, attributes: attributes)
        self.setAttributedTitle(attributedString, forState: .Normal)
    }
}