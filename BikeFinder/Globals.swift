//
//  Globals.swift
//  BikeFinder
//
//  Created by Janus fidel Balatbat on 6/2/16.
//  Copyright © 2016 Janus fidel Balatbat. All rights reserved.
//

import UIKit


class Globals: NSObject {
    class func addDonebuttonToTextField(textField: UITextField, selector: Selector, viewCon: UIViewController){
        let doneButton : UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: viewCon, action: selector)
        doneButton.tintColor = UIColor.whiteColor()
        let toolBar : UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: viewCon.view.frame.width, height: 40))
        toolBar.barStyle = UIBarStyle.BlackTranslucent;
        toolBar.tintColor = UIColor.whiteColor();
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.FlexibleSpace, target: nil, action: nil), doneButton]
        textField.inputAccessoryView = toolBar
    }
    
    
    class func showMessageDialog(title: String, message:String,completion:(done: Bool)->Void){
        if objc_getClass("UIAlertController") != nil {
            let alertCon = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
                completion(done: true)
            })
            alertCon.addAction(okAction)
            dispatch_async(dispatch_get_main_queue(), {
                UIApplication.topViewController()?.presentViewController(alertCon, animated: true, completion: { () -> Void in
                    
                })
                
            })
            
            
        }else{
            
        }
        
        
    }

}
